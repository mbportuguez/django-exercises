from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse
from django.shortcuts import redirect
from rest_framework import serializers



class Post(models.Model):
    title = models.CharField(max_length=100)
    content = models.TextField()
    date_posted = models.DateTimeField(default=timezone.now)
    author = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    # create a get absolute url method
    # so that django_activity will know how to find the location of a specific post
    # use reverse to use the full url to that route as a string


    def get_absolute_url(self):
        # return a path to a specific post
        # reverse(<url destination, kwargs={<url parameter>: <instance of a specific post>}>
        return reverse('post-detail', kwargs={'pk': self.pk})

class Comment(models.Model):
    message = models.TextField()
    date_posted = models.DateTimeField(default=timezone.now)

    # use ForeignKey to use one to many relationship in django_activity
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.message

    def serialize(model, many=False):
        if many:
            return map(Comment.serializer, model)
        return Comment.serializer(model)

    def serializer(model):
        dict_model = model_to_dict(model)
        return dict_model

    def get_absolute_url(self):
        # return a path to a specific post
        # reverse(<url destination, kwargs={<url parameter>: <instance of a specific post>}>
        return reverse('blog-home')

#--------------------------------------------------------------------------
# class OtherParty(models.Model):
#     accident = models.ForeignKey(Accident)
#     other_party_name = models.CharField(max_length=100)
#     other_party_drivers_license_num = models.CharField(max_length=100)
#     other_party_license_plate_num = models.CharField(max_length=100)
#     other_party_contact_num = models.CharField(max_length=100)
#     other_party_damage_loc = models.TextField()
#     other_party_damage_comments = models.TextField()
#     other_party_date_added = models.DateField(auto_now_add=True, null=True)
#
#     def __unicode__(self):
#         return "%s - %s" % (self.id,self.other_party_name)
#
#     @staticmethod
#     def serialize(model, many=False):
#         if many:
#             return map(OtherParty.serializer, model)
#         return OtherParty.serializer(model)
#
#     @staticmethod
#     def serializer(model):
#         dict_model = model_to_dict(model)
#         return dict_model