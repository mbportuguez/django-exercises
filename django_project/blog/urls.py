from django.urls import path, include
from .views import (
    PostListView,
    PostDetailView,
    PostCreateView,
    PostUpdateView,
    PostDeletelView,
    CommentCreateView,
    comment_delete,
)





urlpatterns = [
    #######################
    #
    # routing using function based view
    #
    ######################

    # path('', views.home , name='blog-home'),

    ######################
    #
    # routing using class based views
    #
    ########################

    # add .as_view() when referencing to class based view
    path('', PostListView.as_view(), name='blog-home'),

    # route to a specific post using primary key or pk and what kind of variable it is <int:pk>
    path('post/<int:pk>/', PostDetailView.as_view(), name='post-detail'),

    # to be able to use this route, create a template
    # using this format <model name>_form.html
    # ex. post_form.html
    path('post/new/', PostCreateView.as_view(), name='post-create'),

    # just pass in the primary key of the post and all will be handled by the view method
    path('post/<int:pk>/update/', PostUpdateView.as_view(), name='post-update'),

    path('post/<int:pk>/delete/', PostDeletelView.as_view(), name='post-delete'),

    path('comment/<int:pk>/new/', CommentCreateView.as_view(), name='comment-create'),


    path('comment/<int:pk>/delete', comment_delete, name='comment-delete'),


]

