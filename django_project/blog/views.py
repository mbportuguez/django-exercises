from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)
from .models import Post, Comment
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse


from django.http import HttpResponse

from django.db import models

#############################
#
#   function based views
#
#############################

#
# def home(request):
#     context = {
#         'posts': Post.objects.all()
#     }
#     return render(request, 'blog/home.html', context)

##############################
#
#   class based views
#
#############################


class PostListView(ListView):
    model = Post  # what model to query

    # this is the default TEMPLATE NAME format when using views
    # <app name>/<model>_<view type>.html
    # example: blog/post_list.html
    # when not using the default TEMPLATE NAME format
    # use below format to reference predefined template
    template_name = 'blog/home.html'

    context_object_name = 'posts'  #
    # #  context_object_name is the same as below example
    # context = {
    #         'posts': Post.objects.all()
    #     }
    #     return render(request, 'blog/home.html', context)

    # order of query by date from latest post


    # query a foreign
    # def get_queryset(self):
    #     queryset = (Comment.objects
    #                 .all()
    #                 .prefetch_related('message')
    #                 .order_by('-date_posted'))
    #     return queryset

    ordering = ['-date_posted']


class PostDetailView(DetailView):
    # all we need is to specify that Model
    # and the functions are automatically generated
    model = Post


# LoginRequiredMixin argument is an alternative for login decorator for function based views
# it means adding a functionality to a view telling that a
# user can never add a post when not logged in
class PostCreateView(LoginRequiredMixin, CreateView):
    model = Post

    # this is the field that would be populated in the model
    # and will automatically generated in the template
    fields = ['title', 'content']

    # in the model we have an AUTHOR field and it cannot be null
    # so we need to override form_valid method to update the author to be the current logged in user
    def form_valid(self, form):  # form valid takes a self, and form as an argument

        # before its submitted take that instance and
        # set the author to be the logged in user
        form.instance.author = self.request.user

        # validated the form and pass the form as an argument
        return super().form_valid(form)

    # it will create the post successfully but it will create an error
    # because it didn't know where to redirect when submitted.
    # we need to redirect to a certain url once the post is created
    # steps:
    # create a get absolute url method to path of specific instance
    # go to Post model to create a specific url


# UserPassesTestMixin is a test for users.
# this only means that if the author of the post is not the current logged in user
# it cannot update that post
class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Post
    fields = ['title', 'content']

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    # from UserPassesTestMixin
    # test a user
    def test_func(self):
        # get the exact post that will be updated by using .get_object()
        post = self.get_object()

        # check if the author is currently the logged in user
        if self.request.user == post.author:

            # return true if the author is currently
            # the logged in user and allow the user to update the post
            return True

        # else return false if its not and it will never be updated
        # and will return an error
        return False


class PostDeletelView(LoginRequiredMixin, UserPassesTestMixin, DeleteView): # all mixins will be to the left of the views inheritance
    model = Post

    # add success_url to redirect when its deleted
    success_url = '/'

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False


class CommentCreateView(LoginRequiredMixin, CreateView):

    model = Comment
    fields = ['message']

    def form_valid(self, form):

        # return HttpResponse("1")
        # user_data = request.data

        form.instance.author = self.request.user

        form.instance.post = Post.objects.get(pk=self.kwargs['pk'])

        # return HttpResponse(self.kwargs['pk'])
        #
        # form.instance.post =  # get the value of pk in url
        # r = self.kwargs['pk']
        # comments = Comment.object.filter(id=pk)
        # comments.message = user_data['message']
        # comments.save()
        # list rotue
        # detail route

        return super().form_valid(form)
        # return r

@login_required
def comment_delete(request, pk):

    comment = Comment.objects.get(id=pk)
    comment.delete()

    return render(request, 'blog/home.html', {'message': 'comment deleted'})
    # return HttpResponse(comment)


