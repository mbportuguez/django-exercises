from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Profile



class UserRegisterForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    last_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')
    birthday = forms.DateField(help_text='format is mm/dd/yyyy ')

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'birthday', 'email', 'password1', 'password2']

class UserUpdateForm(forms.ModelForm):
    first_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    last_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')
    birthday = forms.DateField(help_text='format is mm/dd/yyyy ')

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'birthday', 'email']

# class ProfileUpdateForm(forms.ModelForm):
#     pass
#
#     # class Meta:
#     #     model = Profile

